# Introduction

It's been over 700 years since The Avatar, shielded by the Barrier of Light, cast the Armageddon spell and fused themselves with The Guardian and neutralized the evil power that had corrupted the land. Lord British has left the lands on a temporal jaunt across space-time and left the lands to heal.

After the great cataclysm, Britannia is now whole again and the people have been rebuilding for some time. The stories are told of The Avatar, The Cataclysm, and Lord British's embodiment of The Virtues but they are seen more as myth and legend rather than history. Castle Britannia was ordered to be kept ever-ready in case Lord British returns to rest upon his throne in Britannia's time of need and The Virtues are tended to daily.

Not all has been easy for the people of Britannia though. There have been famines, skirmishes amongst city-states, and still grotesque creatures have been seen roaming the lands. The bands of robed magic users wielding massive halberds still traverse the lands robbing, killing, and thieving.Normalcy in all things seems to have settled on much of the land, and the dead have begun to rise in the Britain Graveyard, with many speculating a liche has risen from the bowels of Deceit to sow discord throughout the land.

## Call to Action

Hail Noble Warriors! Our land is in need of stalwart heroes who are willing to brave perils too horrific for some to consider. Necromancy has appeared near the city again and has become a scourge for the local townsfolk on the north edge of town. The surrounding homes lie sacked, where once the mourning came to grieve they are now greeted with the specters and skeletons of loved ones long passed.

A man calling himself Lord Courtry has been politicking across the lands in Minoc, Valoria, and Yew. He claims to be descended from Lord British, though it is curious how he has obtained his wealth. There's a rumor he has been monopolizing and hording a reagent needed by the many mages of Britannia and forcing them to work for him for access to this necessity.

A band of orcs has been sighted outside Britain's mines at the crossroads. It is strange that they have encroached there as there is a well known band of murderers who pass through this area semi-regularly but they have not been seen for several months. There are reports but nothing substantiated as of yet.
