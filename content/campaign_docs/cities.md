# Cities of Sosaria

* [Britain - City of Compassion](#britain)
* [Buccaneer's Den - City of Pirates and Thieves](#buccaneer-s-den)
* [Cove - City of Love](#cove)
* [Jhelom - City of Valor](#jhelom)
* [Magincia - City of Pride](#magincia)
* [Minoc - City of Sacrifice](#minoc)
* [Moonglow - City of Honesty](#moonglow)
* [Nujel'm - City of Pleasure](#nujel-m)
* [Occlo - Mountain People](#occlo)
* [Serpent's Hold - Order of the Silver Serpent](#serpent-s-hold)
* [Skara Brae - City of Sprituality](#skara-brae)
* [Trinsic - City of Honor](#trinsic)
* [Vesper - City of Industry](#vesper)
* [Wind - City of Magicka](#wind)
* [Yew - City of Justice](#yew)

## Britain

__**Virtue**__ - Compassion

__**Government**__ - Incumbent Monarchy of Lord British(Abandoned), Parliamentary

__**Imports**__ - Luxury Goods, Exotic Goods

__**Exports**__ - Basic Necessities

### Geography

### Climate

### Culture

## Buccaneer's Den

__**Virtue**__ - Thievery

__**Government**__ - Pirate Conclave

__**Imports**__ - Blackmarket Goods, Basic Necessities(10x Markup)

__**Exports**__ - Luxury Goods, Exotic Goods

### Geography

### Climate

### Culture

## Cove

__**Virtue**__ - Love

__**Government**__ - Village Structure - Group of Elders make community decisions, Garrisoned by Britain Military

__**Imports**__ - Alcohol

__**Exports**__ - Arms and Armor, Crafted Items

### Geography

### Climate

### Culture

## Jhelom

__**Virtue**__ - Valor

__**Government**__ - Tribunal of five elected officials from the various islands and primary land structure

__**Imports**__ - Food, textiles, and rare minerals

__**Exports**__ - Mercenaries, War Horses

### Geography

### Climate

### Culture

## Magincia

__**Virtue**__ - Pride
__**Government**__ - Plutocracy consisting of nine members
__**Imports**__ -
__**Exports**__ - Mercenaries, War Horses

### Geography

### Climate

### Culture
